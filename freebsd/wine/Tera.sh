#!/usr/bin/env sh

##  Path of Wine to be used
WINE="/usr/local/wine-proton/bin/wine64"
export WINE

## Specify a WINEPREFIX
export WINEPREFIX="$HOME/.wineprefix/TERA"

## Specify ARCH (win32 or win64)
export WINEARCH=win64

## Create or update WINEPREFIX (if needed)
# "$WINE" wineboot -u

## Install addititions to the WINEPREFIX

## Base changas
# winetricks allfonts faudio dxvk1103 d3dx11_43 vkd3d vcrun2005sp1
## .dotnet and MS-XML
# winetricks msxml6 dotnet48
## VC Runtime
# winetricks vcrun2022 vcrun2013 vcrun2012 comdlg32ocx
## MFC
# winetricks mfc140 mfc120 mfc110 mfc100
## OLE
# winetricks ole32 oleaut32
## Internet Exploder Changas, must be installed like this
# winetricks ie8
# winetricks ie8_kb2936068
# winetricks ie_tls12
## Specific settings
# winetricks usetakefocus=n mwo=disable
## Run winetricks in the end if needed
# winetricks

## Extra variables
export PROTON_FORCE_LARGE_ADDRESS_AWARE='1'
export STAGING_SHARED_MEMORY='1'
export STAGING_WRITECOPY='1'
export WINEDEBUG='-all'

## This is useful if you have a nvidia card
# export __GL_SHADER_CACHE='1'
# export __GL_SHADER_DISK_CACHE_PATH='$HOME/.wineprefix/TERA/'
# export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP='1'
# export __GL_THREADED_OPTIMIZATIONS='1'

## Game launch
"$WINE" "$HOME/Games/TERA/Tera Starscape.exe"

#### Wine config if necessary
# winecfg
