#!/usr/bin/env sh

## Path of Wine to be used
WINE="/usr/local/wine-proton/bin/wine"
export WINE

## Specify a WINEPREFIX
export WINEPREFIX="$HOME/.wineprefix/TagScanner"

## Specify ARCH if needed
export WINEARCH=win32

## Create or update WINEPREFIX (if needed)
# "$WINE" wineboot -u

## Install fonts
# winetricks corefonts usetakefocus=n

## Extra variables
export WINEDEBUG="-all"

## Launch
"$WINE" "$HOME/Games/TagScanner/Tagscan.exe"
