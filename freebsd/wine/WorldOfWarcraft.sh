#!/usr/bin/env sh

## Path of Wine to be used
WINE="/usr/local/wine-proton/bin/wine64"
export WINE

## Specify a WINEPREFIX
export WINEPREFIX="$HOME/.wineprefix/WoW"

## Specify ARCH (win32 or win64)
export WINEARCH=win64

## Create or update WINEPREFIX (if needed)
# "$WINE" wineboot -u

## Install addititions to the WINEPREFIX
# winetricks corefonts faudio dxvk usetakefocus=n mwo=disable d3dx11_43 vkd3d vcrun2005sp1
# winetricks

## Extra variables
export PROTON_FORCE_LARGE_ADDRESS_AWARE='1'
export STAGING_SHARED_MEMORY='1'
export STAGING_WRITECOPY='1'
export WINEDEBUG='-all'

## This is useful if you have a nvidia card
# export __GL_SHADER_CACHE='1'
# export __GL_SHADER_DISK_CACHE_PATH='$HOME/.wineprefix/WoW/'
# export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP='1'
# export __GL_THREADED_OPTIMIZATIONS='1'

## Game launch
"$WINE" "$HOME/Games/WoW/Wow.exe"

## Wine config if necessary
# winecfg
