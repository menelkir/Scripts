#!/usr/bin/env sh

## Path of Wine to be used
WINE="/usr/local/wine-proton/bin/wine64"
export WINE

## Specify a WINEPREFIX
export WINEPREFIX="$HOME/.wineprefix/StarRail"

## Specify ARCH (win32 or win64)
export WINEARCH=win64

## Create or update WINEPREFIX (if needed)
# "$WINE" wineboot -u

## Install DXVK and corefonts
# winetricks allfonts faudio dxvk usetakefocus=n mwo=disable
# winetricks

## Extra variables
export WINEESYNC=1
export WINE_LARGE_ADDRESS_AWARE=1
export PROTON_LARGE_ADDRESS_AWARE=1
export WINEDEBUG=-all
export STAGING_SHARED_MEMORY=1
export STAGING_WRITECOPY=1
export PROTON_ENABLE_NVAPI=1
export PROTON_NO_WRITE_WATCH=1
export VKD3D_CONFIG="force_static_cbv,force_bindless_texel_buffer"

## This is useful if you have a nvidia card
# export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP=1
# export __GL_THREADED_OPTIMIZATIONS=1
# export __GL_SHADER_DISK_CACHE=1
# export __GL_SHADER_DISK_CACHE_PATH=$HOME/.wineprefix/StarRail

## Game launch
"$WINE" "$HOME/Games/StarRail/Launcher/launcher.exe"

## Wine config if necessary
# "$HOME/.local/share/honkers-launcher/runners/lutris-GE-Proton8-25-x86_64/bin/winecfg"

