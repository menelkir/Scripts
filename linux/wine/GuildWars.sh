#!/usr/bin/env sh
## Path of Wine to be used
WINE="/usr/bin/wine"
export WINE

## Specify a WINEPREFIX
export WINEPREFIX="/home/menelkir/.wineprefix/GuildWars"

## Specify ARCH if needed
export WINEARCH=win32

## Create or update WINEPREFIX (if needed)
#"$WINE" wineboot -u

## Install addititions to the WINEPREFIX
#winetricks dxvk corefonts faudio usetakefocus=n
#winecfg

## Extra variables
export STAGING_SHARED_MEMORY='1'
export STAGING_WRITECOPY='1'
export WINEDEBUG='-all'

## Variables for wayland
export MESA_VK_WSI_PRESENT_MODE=immediate
export vk_xwayland_wait_ready=false

## This is useful if you have a nvidia card
# export __GL_SHADER_CACHE='1'
# export __GL_SHADER_DISK_CACHE_PATH= '/home/menelkir/.wineprefix/GuildWars'
# export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP='1'
# export __GL_THREADED_OPTIMIZATIONS='1'

## Game launch
cd "$HOME/Games/Guild Wars"
"$WINE" $HOME/Games/Guild\ Wars/Gw.exe --character "Daniel Menelkir" --email "menelkir@tutanota.com" --password "M3up4ud3luv4!@#"
