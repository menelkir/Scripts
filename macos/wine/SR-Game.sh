#!/usr/bin/env sh

#### Path of Wine to be used
# CROSSOVER WINE
# This needs CrossOver to be installed and had a first run
WINE="/Applications/CrossOver.app/Contents/SharedSupport/CrossOver/CrossOver-Hosted Application/wineloader64"

# WINECX
# WiNE="/usr/local/winecx/bin/wine64"
# export WINE
# WINEDLLPATH="/usr/local/winecx/lib/wine"
# export WINEDLLPATH

# Wine Stable
# WINE="/Applications/Wine Stable.app/Contents/Resources/start/bin/wine64"

#### Specify a WINEPREFIX
# This is where the wineprefix needs to be. Note jadeite should also be in this folder.
export WINEPREFIX="$HOME/Library/Application Support/WinePrefix/StarRail"

#### Specify ARCH (win32 or win64)
export WINEARCH=win64

#### Create or update WINEPREFIX (if needed)
# Can be commented after the first run
# "$WINE" wineboot -u

#### Specific macOS variables
## MSYNC is part of wine-staging, crossover will support it at some point.
export WINEMSYNC=1
## XXX required by Wine since it doesn't handle VK_ERROR_DEVICE_LOST correctly
export MVK_CONFIG_RESUME_LOST_DEVICE=1

#### Other useful variables
export STAGING_WRITECOPY=1
export STAGING_SHARED_MEMORY=1
export STAGING_RT_PRIORITY_SERVER=90
export STAGING_RT_PRIORITY_BASE=90

#### Game launch
"$WINE" "$HOME/Library/Application Support/WinePrefix/jadeite/jadeite.exe" "$HOME/Games/StarRail/Game/StarRail.exe" -- -disable-gpu-skinning
#"$WINE" winecfg
