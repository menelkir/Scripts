#!/usr/bin/env sh

# Disable macOS annoyances
#

# Disable auto-switching workspaces
	defaults write com.apple.dock workspaces-auto-swoosh -bool NO

# Don't offer new disks for Time Machine backup
	defaults write com.apple.TimeMachine "DoNotOfferNewDisksForBackup" -bool "false"

# When switching to an app, don’t switch to a space with open windows for this app.
	defaults write NSGlobalDomain "AppleSpacesSwitchOnActivate" -bool "false"
	defaults write com.apple.dock workspaces-auto-swoosh -bool FALSE

# Group windows by application
	defaults write com.apple.dock "expose-group-apps" -bool "true"

# Do not rearrange spaces by usage
	defaults write com.apple.dock "mru-spaces" -bool "false"

# Disable question about opening applications downloaded from the internet
	defaults write com.apple.LaunchServices LSQuarantine -bool false

# Avoid creating .DS_Store files on network or USB volumes
	defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
	defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

# Turn on subpixel rendering for non-retina displays
	defaults write -g CGFontRenderingFontSmoothingDisabled -bool NO

# Disable disk image verification
	defaults write com.apple.frameworks.diskimages skip-verify -bool true
	defaults write com.apple.frameworks.diskimages skip-verify-locked -bool true
	defaults write com.apple.frameworks.diskimages skip-verify-remote -bool true

# Don’t show recent applications in Dock
	defaults write com.apple.dock show-recents -bool false

# Privacy: don’t send search queries to Apple
	defaults write com.apple.Safari UniversalSearchEnabled -bool false
	defaults write com.apple.Safari SuppressSearchSuggestions -bool true

# Don’t display the annoying prompt when quitting iTerm
	defaults write com.googlecode.iterm2 PromptOnQuit -bool false

# Disable local Time Machine backups
#	hash tmutil &> /dev/null && sudo tmutil disablelocal

# Prevent Photos from opening automatically when devices are plugged in
	defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true

# Turn Sleep into hibernation
	sudo pmset -a hibernatemode 25

# Accelerate copy to devices by disabling filesystem throttling
	sudo sysctl debug.lowpri_throttle_enabled=0

# Enable Retina on non-retina displays
	sudo defaults write /Library/Preferences/com.apple.windowserver.plist DisplayResolutionEnabled -bool true

# Always Show Library
	chflags nohidden ~/Library/

# Show Debug Menus
	defaults write com.apple.Safari IncludeInternalDebugMenu 1
	defaults write com.apple.Safari IncludeDevelopMenu -bool true
	defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true
	defaults write NSGlobalDomain WebKitDeveloperExtras -bool true
	defaults write com.apple.appstore ShowDebugMenu -bool true
	defaults write com.apple.DiskUtility advanced-image-options 1
	defaults write com.apple.DiskUtility DUDebugMenuEnabled 1

# Disable login wallpaper
	defaults write /Library/Preferences/com.apple.loginwindow DesktopPicture ""

# Disable desktop widgets on Sonoma
	defaults write com.apple.WindowManager StandardHideWidgets -int 0
	defaults write com.apple.WindowManager StageManagerHideWidgets -int 0

# Disable click desktop to show desktop on Sonoma
	defaults write com.apple.WindowManager EnableStandardClickToShowDesktop 0

# Highlight hidden items on dock
	defaults write com.apple.Dock showhidden

# Disable sudden motion sensor
	sudo pmset -a sms 0

# Make safari show full URL
	defaults write com.apple.Safari "ShowFullURLInSmartSearchField" -bool "true" && killall Safari

# Show hidden files on finder
#	defaults write com.apple.finder "AppleShowAllFiles" -bool "true"

# Keep folders on top on finder
#	defaults write com.apple.finder "_FXSortFoldersFirst" -bool "true"

# Disable warning when rename files
	defaults write com.apple.finder "FXEnableExtensionChangeWarning" -bool "false"

# Title bar icons on finder
	defaults write com.apple.universalaccess "showWindowTitlebarIcons" -bool "true"

# Show icons on desktop
	defaults write com.apple.finder "ShowHardDrivesOnDesktop" -bool "true"
	defaults write com.apple.finder "ShowMountedServersOnDesktop" -bool "true"

# Flash clock separators
	defaults write com.apple.menuextra.clock "FlashDateSeparators" -bool "true"

# Enable dragging with three fingers
	defaults write com.apple.AppleMultitouchTrackpad "TrackpadThreeFingerDrag" -bool "true"

# Show build durations on Xcode
	defaults write com.apple.dt.Xcode "ShowBuildOperationDuration" -bool "true"

# Don’t show notifications for music
	defaults write com.apple.Music "userWantsPlaybackNotifications" -bool "false"
